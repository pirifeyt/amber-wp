<?php

// Įjungiame post thumbnail

add_theme_support( 'post-thumbnails' );
add_image_size('top-logo', 128, 48, true);
add_image_size('homepage-header-bg-image', 1680, 950, true);
add_image_size('quotes-img', 1500, 500, true);
add_image_size('homepage-works-thumb', 290, 290, true);
add_image_size('homepage-blog-image', 290, 220, true);
add_image_size('our-team-img', 210, 280, true);

add_image_size('single-blog-image', 690, 520, false);
add_image_size('single-blog-image-x350', 350, 350, true);

// Apsibrėžiame stiliaus failus ir skriptus

if( !defined('ASSETS_URL') ) {
	define('ASSETS_URL', get_bloginfo('template_url'));
}


add_action('init', 'amberStartSession', 1);
function amberStartSession() {
    if(!session_id()) {
        session_start();
    }
}

function theme_scripts(){

    if ( !is_admin() ) {

        wp_enqueue_script('jquery');

        if(is_front_page()){
            wp_register_script('circular', ASSETS_URL . '/scripts/js/circular.js', array('jquery'), md5(time()), true);
            wp_enqueue_script('circular');
        }

        if(is_single()){
            wp_register_script('fancybox', ASSETS_URL . '/scripts/js/fancybox.min.js', array('jquery'), '0.1', true);
            wp_enqueue_script('fancybox');
        }

        wp_register_script('g-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAMuFsMVMa57fgIWffyz2pMRXRAzFHldLw', array('jquery'), '', true);
        wp_register_script('slick', ASSETS_URL . '/scripts/js/slick.js', array('jquery'), md5(time()), true);
        wp_register_script('amber', ASSETS_URL . '/scripts/js/amber.js', array('jquery'), md5(time()), true);

        wp_enqueue_script('g-maps');
        wp_enqueue_script('slick');
        wp_enqueue_script('amber');
    }
}

add_action('wp_enqueue_scripts', 'theme_scripts');


function theme_stylesheets(){

	$styles_path = ASSETS_URL . '/assets/css/main.css';
	if( $styles_path ) {

        if(is_single()){
            wp_register_style('fancybox', ASSETS_URL . '/assets/css/fancybox.min.css', array(), '0.1', 'all');
            wp_enqueue_style('fancybox');
        }

		wp_register_style('slick', ASSETS_URL . '/assets/css/slick.css', array(), md5(time()), 'all');
		wp_register_style('slick-theme', ASSETS_URL . '/assets/css/slick-theme.css', array('slick'), md5(time()), 'all');
        wp_register_style('style', ASSETS_URL . '/assets/css/style.css', array('slick-theme'), md5(time()), 'all');

		wp_enqueue_style('slick');
		wp_enqueue_style('slick-theme');
		wp_enqueue_style('style');
	}
}
add_action('wp_enqueue_scripts', 'theme_stylesheets');

// Apibrėžiame navigacijas

function register_theme_menus() {

	register_nav_menus(array(
        'primary-navigation' => __( 'Primary Navigation' )
    ));
}


add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'current-item ';
    }
    return $classes;
}

add_action( 'init', 'register_theme_menus' );

// Apibrėžiame widgets juostas

$sidebars = array( 'Latest Tweets Widget Footer', 'Blog Tags' );

if( isset($sidebars) && !empty($sidebars) ) {

	foreach( $sidebars as $sidebar ) {

		if( empty($sidebar) ) continue;

		$id = sanitize_title($sidebar);

		register_sidebar(array(
			'name' => $sidebar,
			'id' => $id,
			'description' => $sidebar,
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		));
	}
}

// Custom posts

$themePostTypes = array(
    'Works' => 'Work'
);

function createPostTypes() {

	global $themePostTypes;

	$defaultArgs = array(
		'taxonomies' => array('category'), // uncomment this line to enable custom post type categories
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		#'menu_icon' => '',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'has_archive' => true, // to enable archive page, disabled to avoid conflicts with page permalinks
		'menu_position' => null,
		'can_export' => true,
		'supports' => array( 'title', 'editor', 'thumbnail', /*'custom-fields', 'author', 'excerpt', 'comments'*/ ),
	);

	foreach( $themePostTypes as $postType => $postTypeSingular ) {

		$myArgs = $defaultArgs;
		$slug = sanitize_title( $postType );
		$labels = makePostTypeLabels( $postType, $postTypeSingular );
		$myArgs['labels'] = $labels;
		$myArgs['rewrite'] = array( 'slug' => $slug, 'with_front' => true );
		$functionName = 'postType' . $postType . 'Vars';

		if( function_exists($functionName) ) {

			$customVars = call_user_func($functionName);

			if( is_array($customVars) && !empty($customVars) ) {
				$myArgs = array_merge($myArgs, $customVars);
			}
		}

		register_post_type( $postType, $myArgs );

	}
}

if( isset( $themePostTypes ) && !empty( $themePostTypes ) && is_array( $themePostTypes ) ) {
	add_action('init', 'createPostTypes', 0 );
}


function makePostTypeLabels( $name, $nameSingular ) {

	return array(
		'name' => _x($name, 'post type general name'),
		'singular_name' => _x($nameSingular, 'post type singular name'),
		'add_new' => _x('Add New', 'Example item'),
		'add_new_item' => __('Add New '.$nameSingular),
		'edit_item' => __('Edit '.$nameSingular),
		'new_item' => __('New '.$nameSingular),
		'view_item' => __('View '.$nameSingular),
		'search_items' => __('Search '.$name),
		'not_found' => __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Bin'),
		'parent_item_colon' => ''
	);
}

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();

}

function custom_excerpt($text='', $length=200) {
    $text = strip_tags($text.'');
    return strlen($text) > $length ? substr($text, 0 , $length).'[..]' : $text;

}


function my_acf_init() {

    acf_update_setting('google_api_key', 'AIzaSyAMuFsMVMa57fgIWffyz2pMRXRAzFHldLw');
}

add_action('acf/init', 'my_acf_init');


function pre($text, $exit = 0){
    echo '<pre>'.print_r($text, true).'</pre>';
    if($exit) exit;
}









add_action( 'init', 'codex_blog_init' ); //Register blog post type

function codex_blog_init() {
    $labels = array(
        'name'               => _x( 'Blog', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Blog', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Blog', 'admin menu', 'blog' ),
        'name_admin_bar'     => _x( 'Blog', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New Blog', 'blog', 'blog' ),
        'add_new_item'       => __( 'Add New Blog', 'blog' ),
        'new_item'           => __( 'New Blog', 'blog' ),
        'edit_item'          => __( 'Edit Blog', 'blog' ),
        'view_item'          => __( 'View Blog', 'blog' ),
        'all_items'          => __( 'All Blogs', 'blog' ),
        'search_items'       => __( 'Search Blog', 'blog' ),
        'parent_item_colon'  => __( 'Parent Blog:', 'blog' ),
        'not_found'          => __( 'No Blog found.', 'blog' ),
        'not_found_in_trash' => __( 'No Blog found in Trash.', 'blog' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'blog' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'blog' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt')
    );

    register_post_type( 'blog', $args );
}


// hook into the init action and call create_taxonomies when it fires

add_action( 'init', 'create_taxonomies', 0 );

function create_taxonomies() {
    // Add new taxonomy, make it hierarchical (like categories)

    $taxonomies = [
        ['name' => 'Blog Tags', 'singular_name' =>'Blog Tag', 'taxonomie_name' => 'blog', 'slug'=> 'blog-tag'],
        ['name' => 'Blog Categories', 'singular_name' =>'Blog Category', 'taxonomie_name' => 'blog', 'slug'=>'blog-category'],
    ];

    foreach ($taxonomies as $t){
        $labels = array(
            'name'              => _x( $t['name'],                      'taxonomy general name', $t['taxonomie_name']),
            'singular_name'     => _x( $t['singular_name'],             'taxonomy singular name', $t['taxonomie_name'] ),
            'search_items'      => __( 'Search '.$t['name'],            $t['taxonomie_name'] ),
            'all_items'         => __( 'All '.$t['name'],               $t['taxonomie_name'] ),
            'parent_item'       => __( 'Parent ' . $t['singular_name'], $t['taxonomie_name'] ),
            'parent_item_colon' => __( 'Parent Genre:',                 $t['taxonomie_name'] ),
            'edit_item'         => __( 'Edit ' . $t['singular_name'],   $t['taxonomie_name'] ),
            'update_item'       => __( 'Update ' . $t['singular_name'], $t['taxonomie_name'] ),
            'add_new_item'      => __( 'Add New ' . $t['singular_name'],$t['taxonomie_name'] ),
            'new_item_name'     => __( 'New ' . $t['singular_name'] . ' Name', $t['taxonomie_name'] ),
            'menu_name'         => __( $t['singular_name'],             $t['taxonomie_name'] ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => ['slug' => $t['slug']],
        );

        register_taxonomy( $t['slug'], array( $t['taxonomie_name'] ), $args );
    }

}




?>