<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tiialt_amberwp');

/** MySQL database username */
define('DB_USER', 'tiialt_amber');

/** MySQL database password */
define('DB_PASSWORD', 'tiialt_amber.password321');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('PLL_CACHE_HOME_URL', false);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Czm4P.K[4E?J]2a(7w.sD3O<an_1vRMNs`;{hbK<Np.IDV_;^Q6vPg9RkiB!SyFz');
define('SECURE_AUTH_KEY',  '5R/Z<iu#uJ]-{!Z$>5w84{Urf)iRZRYGezIkF,EN];(2{20O}tD62Kf.+JUB$0s:');
define('LOGGED_IN_KEY',    'Wr_h$r<z;MFb*-O>Cc1jo]fTO&fgR.|7*w.UPiigi!37Q#8/u~oT8)*z[b*,W>gk');
define('NONCE_KEY',        '%b$8,vape$=bJB(QIIcNw%F q|A%/[Cvsmm~O@9(1%#1:1;pV?!m]lQR4b8(/.q(');
define('AUTH_SALT',        '_R>MB}%4D@94E)!V&}nI!pQq.R3oMU5=vX:c+z$6W}HO:cS/AvMg|6CvpbJXs-u:');
define('SECURE_AUTH_SALT', '_v63}u)4,*#}pWMV ^5cU6.(Ue~XzshRd^Nh8Dw.K{/.$|!iNqmsA&#W b{WYEen');
define('LOGGED_IN_SALT',   'R053aRbF$Hi20XtW)zf6XY)i3cJV2PA}o-ofZJc/mq4FX_.U3]i$Q}9Dg0mZ*,l)');
define('NONCE_SALT',       '9y0r@fL;Tjw6|7RgMAE]OZLH3kFVPx1yixv.>!hBcc<+]m]k$kS^3uJ0kvEVRIVr');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
